""" DBConnector class for connectivity to various databases """

__author__ = 'Shlok Chaudhari'
__all__ = 'connector'


import xpresso.ai.core.commons.utils.constants as constants
import xpresso.ai.core.commons.exceptions.xpr_exceptions as xpr_exp
from xpresso.ai.core.data.connections.abstract_db import AbstractDBConnector
from xpresso.ai.core.data.connections.external.presto import client as presto
from xpresso.ai.core.logging.xpr_log import XprLogger


class PrestoDBConnector(AbstractDBConnector):
    """

    DataConnector class for connecting to the databases.

    """

    def __init__(self):
        """

        __init__() here initializes the client object needed for
        interacting with datasource API.

        """

        self.client = None

    def getlogger(self):
        """

        Xpresso logger built on top of python module.

        Returns:
            object: xpresso.ai logger module.

        """

        return XprLogger()

    def connect(self, config):
        """

        Connect method to establish client-side connection with a database.

        Args:
            config (dict): A JSON object, input by the user, that states the table
                to be imported.

        Returns:
            table (str): target table name.
            columns (str/list): names of columns required, as a comma-separated string.
                Put '*' in case of all columns.

        """

        self.client = presto.PrestoConnector(config.get(constants.DSN))
        table = config.get(constants.table)
        columns = config.get(constants.columns)
        return table, columns

    def import_files(self, config):
        """

        DBConnector does not support importing files.

        """

        raise xpr_exp.InvalidMethodCall

    def import_dataframe(self, config, **kwargs):
        """

        Importing data from a user specified table in a database.

        Args:
            config (dict): A JSON object, input by the user, that
                states the table to be imported.

        Returns:
            object: a pandas DataFrame.

        """

        table, columns = self.connect(config)
        try:
            data_frame = self.client.import_data(table, columns)
            self.getlogger().info("Data imported from database.")
        except Exception:
            self.getlogger().error("Data import failed")
            raise xpr_exp.DataConnectionsException
        finally:
            self.close()
        return data_frame

    def close(self):
        """

        Method to close connection to database

        """

        self.client.close()
